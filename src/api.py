# Standard imports
import uvicorn
from fastapi import FastAPI

# Constants
APP = FastAPI()


@APP.get("/hiphip")
def hiphip():
    return "Horray!"


@APP.get("/healthcheck")
async def healthcheck():
    return "Health - OK"



if __name__ == "__main__":
    uvicorn.run(
        app = "api:APP",
        host = "0.0.0.0",
        port = 1234,
        reload = True,          # Auto-reload on code changes
    )
